'use strict'

const { makeExecutableSchema } = require('graphql-tools')
const resolvers = require('./resolvers')

// Define our schema using the GraphQL schema language

const typeDefs = `
  type User {
    id: Int!
    username: String!
    email: String!
    city_id:Int!
    posts: [Post]
    city:City!
  }
  type Post {
    id: Int!
    title: String!
    slug: String!
    content: String!
    user: User!
  }
  type City {
    id: Int!
    name: String!
    cityState: State!
  }
  type State {
    id: Int!
    name: String!
    stateCountry: Country!
  }
  type Country {
    id: Int!
    name: String!
  }

  type PropertyData {
    _id: String
    L_ListingID: Int!
    L_Class: String!
    L_Type: String!
  }

  type Query {
    allUsers(page1:Int,perpage1:Int):[User]
    property:PropertyData
    fetchUser(id: Int!): User
    allPosts: [Post]
    fetchPost(id: Int!): Post
  }
  type Mutation {
    login (email: String!, password: String!): String
    createUser (username: String!, email: String!, password: String!, city_id: Int!): User
    addPost (title: String!, content: String!): Post
  }
`

module.exports = makeExecutableSchema({ typeDefs, resolvers })