'use strict'

const User = use('App/Models/User')
const Post = use('App/Models/Post')
const PropertyData = use('App/Models/PropertyData')
const Database = use('Database')

const slugify = require('slugify')

const resolvers = {
  Query: {

    async allUsers(_, { page1, perpage1 }) {
      const users = await User.query().with("posts").with("city.cityState.stateCountry").offset(page1).limit(perpage1).fetch();
      const userJson = JSON.parse(JSON.stringify(users))

      return userJson;
    },

    async property(_, ) {
      const db = await Database.connect('mongodb')
      const result = await db.collection('c_c_a_r').findOne()
      const result1 = await db.collection('c_c_a_r').find().limit( 5 ) 
      // const AllData = await db.raw("select * from c_c_a_r")
      
      console.log("find Result", result)
      // console.log(AllData)
      console.log("limit data",result1)
      // const proeprtyData = await PropertyData.find();
      // console.log(proeprtyData)


      return result;
    },



    async fetchUser(_, { id }) {
      const user = await User.find(id)
      return user.toJSON()
    },

    async allPosts() {
      const posts = await Post.all()
      return posts.toJSON()
    },

    async fetchPost(_, { id }) {
      const post = await Post.find(id)
      return post.toJSON()
    }
  },
  Mutation: {
    async login(_, { email, password }, { auth }) {
      const { token } = await auth.attempt(email, password)
      return token
    },

    async createUser(_, data) {
      const rulesofUser = {
        username: 'required|unique:users,username',
        email: 'required|email|unique:users,email',
        password: 'required'
      }

      const validation = await validateAll(data, rulesofUser);
      if (validation.fails()) {
        throw new Error(validation._errorMessages[0].message);
      }
      else {
        return await User.create(data)
      }
    },

    async addPost(_, { title, content }, { auth }) {
      try {
        await auth.check()
        const user = await auth.getUser()

        const rulesofPost = {
          title: 'required',
          slug: 'unique:posts,slug',
          content: 'required',
        }

        const validation = await validateAll({ title, content }, rulesofPost);
        if (validation.fails()) {
          throw new Error(validation._errorMessages[0].message);
        }
        else {
          return await Post.create({
            user_id: user.id,
            title,
            slug: slugify(title, { lower: true }),
            content
          })
        }
      } catch (error) {
        throw new Error(error)
      }
    }
  },
  User: {
    async posts(userInJson) {
      // Convert JSON to model instance
      const user = new User()
      user.newUp(userInJson)
      const posts = await user.posts().fetch()
      return posts.toJSON()
    }
  },
  Post: {
    // Fetch the author of a particular post
    async user(postInJson) {
      // Convert JSON to model instance
      const post = new Post()
      post.newUp(postInJson)

      const user = await post.user().fetch()
      return user.toJSON()
    }
  }
}

module.exports = resolvers